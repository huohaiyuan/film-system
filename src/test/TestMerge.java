package test;
import POJO.User;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import org.junit.Before;
import org.junit.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import POJO.Person;
import service.LoginService;

import java.util.List;

public class TestMerge {
    ClassPathXmlApplicationContext ctx;
    @Before
    public void loadCtx() {
        // 加载配置文件
        ctx = new ClassPathXmlApplicationContext(
                "applicationContext.xml");
    }
    @Test
    public void testHibernate() {
        SessionFactory sf = (SessionFactory) ctx.getBean("sessionFactory");
        Session session = sf.openSession();
        Transaction transaction = session.beginTransaction();
        session.save(new Person("1","用户1"));
        transaction.commit();
        session.close();
        sf.close();
    }
    /**
     * 测试Spring加载是否正确
     */
    @Test
    public void testSpring(){
        LoginService loginService = (LoginService) ctx.getBean("LoginService");
        List<User> u = loginService.findByName("111");
        System.out.println(u.get(0).getPassWord());
        System.out.println(u.get(0).getUserName());
    }
}
