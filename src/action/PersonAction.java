package action;

import com.opensymphony.xwork2.ActionSupport;
import service.PersonService;

public class PersonAction extends ActionSupport {
    private static final long serialVersionUID = 1L;
    private PersonService personService;
    public PersonService getPersonService() {
        return personService;
    }
    public void setPersonService(PersonService personService) {
        this.personService = personService;
    }
    public String execute() {
        personService.say();
        return SUCCESS;
    }
}