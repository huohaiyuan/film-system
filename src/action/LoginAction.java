package action;

import POJO.User;
import com.opensymphony.xwork2.ActionSupport;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import service.LoginService;

import java.util.List;

public class LoginAction extends ActionSupport {
    private String userName;
    private String passWord;
    ApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml");
    LoginService login = (LoginService) ctx.getBean("LoginService");

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassWord() {
        return passWord;
    }

    public void setPassWord(String passWord) {
        this.passWord = passWord;
    }

    public String execute() throws Exception {
        List<User> userList = login.findByName(userName);
        for (User user : userList) {
            if (user.getPassWord().equals(passWord)) {
                return SUCCESS;
            } else {
                return ERROR;
            }
        }
        return ERROR;
    }
}
