package service;

import POJO.User;
import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.orm.hibernate5.HibernateTemplate;

import java.util.List;

public class LoginServiceImpl implements LoginService {
    private HibernateTemplate ht = null;
    private SessionFactory sessionFactory = null;

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    private HibernateTemplate getHibernateTemplate() {
        if (ht == null) {
            ht = new HibernateTemplate(sessionFactory);
        }
        return ht;
    }

    @Override
    public User get(int id) {
        return getHibernateTemplate().get(User.class, id);
    }

    @Override
    public int save(User user) {
        return (int) getHibernateTemplate().save(user);
    }

    @Override
    public List<User> findByName(String userName) {
        return (List<User>) getHibernateTemplate().find("from User u where u.userName = ?0",userName);
    }
}
