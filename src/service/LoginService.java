package service;

import POJO.User;
import java.util.List;

public interface LoginService {
    User get(int id);
    int save(User user);
    List<User>findByName(String userName);
}
