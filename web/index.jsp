<%--
  Created by IntelliJ IDEA.
  User: 26474
  Date: 2021/11/17
  Time: 15:16
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>mainFrame</title>
    <script src="https://unpkg.com/vue@2.6.11/dist/vue.js"></script>
    <script src="https://unpkg.com/element-ui@2.13.0/lib/index.js"></script>
    <link rel="stylesheet" href="//unpkg.com/element-ui@2.15.6/lib/theme-chalk/index.css">
    <style>
        iframe {
            width: 100%;
            height: 100%;
            border: 0;
        }
        .el-main {
            background-color: #E9EEF3;
            color: #333;
        }
        .el-aside {
            background-color: #E9EEF3;
            color: #333;
        }
    </style>
</head>
<body>
<jsp:include page="jsp/top.jsp" flush="true"></jsp:include>
<div id="mainFrame">
    <el-container style="height: 694px">
        <el-aside>
            <iframe src="jsp/menu.jsp"></iframe>
        </el-aside>
        <el-main style="padding: 0">
            <iframe src="jsp/firstPage.jsp" id="main"></iframe>
        </el-main>
    </el-container>
</div>
</body>
<script>
    new Vue().$mount('#mainFrame')
    var Main = {
        methods: {
            goBack() {
                console.log('go back');
            }
        }
    }
    var Ctor = Vue.extend(Main)
    new Ctor().$mount('#app')
</script>
</html>
