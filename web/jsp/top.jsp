<%--
  Created by IntelliJ IDEA.
  User: 26474
  Date: 2021/11/17
  Time: 8:22
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>TopFrame</title>
    <script src="https://unpkg.com/vue@2.6.11/dist/vue.js"></script>
    <script src="https://unpkg.com/element-ui@2.13.0/lib/index.js"></script>
    <link rel="stylesheet" href="//unpkg.com/element-ui@2.15.6/lib/theme-chalk/index.css">
    <style>
        #top {
            background-color: #B3C0D1;
            font-size: 20px;
            height: 50px;
            display: flex;
            align-items: center;
            border-top-left-radius: 15px;
            border-top-right-radius: 15px;
        }
        span {
            font-family: Serif;
            font-size: 20px;
            margin-left: 20px;
        }
        #service {
            position: absolute;
            right: 20px;
        }
    </style>
</head>
<body>
<div id="top">
    <span>
        <i class="el-icon-s-operation"></i>
        电影后台管理系统
    </span>
    <span id="service">
        欢迎您，huangHao
        <el-link :underline="false" href="login.action"><i class="el-icon-service"></i></el-link>
    </span>
</div>
</body>
<script>
    new Vue().$mount('el-link')
</script>
</html>
