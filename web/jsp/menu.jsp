<%--
  Created by IntelliJ IDEA.
  User: 26474
  Date: 2021/11/17
  Time: 15:11
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>menu</title>
    <script src="https://unpkg.com/vue@2.6.11/dist/vue.js"></script>
    <script src="https://unpkg.com/element-ui@2.13.0/lib/index.js"></script>
    <link rel="stylesheet" href="//unpkg.com/element-ui@2.15.6/lib/theme-chalk/index.css">
<style>
    .el-menu{
        border-radius: 15px
    }
    #menuF{
        border-top-left-radius: 15px;
        border-top-right-radius: 15px;
    }
    #menuE {
        border-bottom-left-radius: 15px;
        border-bottom-right-radius: 15px;
    }
</style>
</head>
<body>
<el-menu :default-openeds="['1','2','3']">
    <el-menu-item id="menuF" index="4" @click="firstPage"><i class="el-icon-s-home"></i>首页</el-menu-item>
    <el-submenu index="1">
        <template slot="title"><i class="el-icon-video-camera"></i>电影管理</template>
        <el-menu-item index="1-1" @click="films"><i class="el-icon-arrow-right"></i>电影库</el-menu-item>
        <el-menu-item index="1-2" @click="filmMessage"><i class="el-icon-arrow-right"></i>信息管理</el-menu-item>
        <el-menu-item index="1-3"><i class="el-icon-arrow-right"></i>类别管理</el-menu-item>
        <el-menu-item index="1-4" @click="filmUpload"><i class="el-icon-arrow-right"></i>电影上传</el-menu-item>
    </el-submenu>
    <el-submenu index="2">
        <template slot="title"><i class="el-icon-chat-line-square"></i>新闻管理</template>
        <el-menu-item index="2-1" @click="news"><i class="el-icon-arrow-right"></i>新闻库</el-menu-item>
        <el-menu-item index="2-2"><i class="el-icon-arrow-right"></i>信息管理</el-menu-item>
        <el-menu-item index="2-3" @click="newUpload"><i class="el-icon-arrow-right"></i>新闻上传</el-menu-item>
    </el-submenu>
    <el-submenu index="3">
        <template slot="title"><i class="el-icon-setting"></i>修改资料</template>
        <el-menu-item index="3-1"><i class="el-icon-arrow-right"></i>修改信息</el-menu-item>
        <el-menu-item id="menuE" index="3-2"><i class="el-icon-arrow-right"></i>修改密码</el-menu-item>
    </el-submenu>
</el-menu>
</body>
<script>
    var app = new Vue({
        el: "el-menu",
        methods: {
            films: function () {
                window.parent.document.getElementById("main").src = "jsp/films.jsp"
            },
            firstPage: function () {
                window.parent.document.getElementById("main").src = "jsp/firstPage.jsp"
            },
            filmMessage: function () {
                window.parent.document.getElementById("main").src = "jsp/filmMessage.jsp"
            },
            filmUpload: function () {
                window.parent.document.getElementById("main").src = "jsp/filmUpload.jsp"
            },
            news: function () {
                window.parent.document.getElementById("main").src = "jsp/news.jsp"
            },
            newUpload: function () {
                window.parent.document.getElementById("main").src = "jsp/newUpload.jsp"
            }
        }
    })
</script>
</html>
