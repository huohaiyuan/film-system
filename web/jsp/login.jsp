<%--
  Created by IntelliJ IDEA.
  User: huanghao
  Date: 2021/5/17
  Time: 17:32
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>登录界面</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/font-awesome.min.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/login.css">
</head>
<body>
<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 320"><path fill="#0099ff" fill-opacity="1" d="M0,192L48,197.3C96,203,192,213,288,229.3C384,245,480,267,576,250.7C672,235,768,181,864,181.3C960,181,1056,235,1152,234.7C1248,235,1344,181,1392,154.7L1440,128L1440,320L1392,320C1344,320,1248,320,1152,320C1056,320,960,320,864,320C768,320,672,320,576,320C480,320,384,320,288,320C192,320,96,320,48,320L0,320Z"></path></svg>
<div class="container">
    <div class="box login">
        <div class="form-content">
            <div class="avtar">
                <div class="pic"><img src="${pageContext.request.contextPath}/img/1.jpg" alt=""></div>
            </div>
            <h1 class="head">account login</h1>
            <s:form action="login" class="form" method="post">
                <div>
                    <i class="fa fa-user-o"></i>
                    <input type="text" placeholder="用户名" name="userName">
                </div>
                <div>
                    <i class="fa fa-key"></i>
                    <input type="password" placeholder="密码" name="passWord">
                </div>
                <div class="btn">
                    <button>登录</button>
                </div>
            </s:form>
            <p class="btn-something">
                还没有账户? <span class="signupbtn">立即注册</span>
            </p>
        </div>
    </div>
    <div class="box signup">
        <div class="form-content">
            <div class="avtar">
                <div class="pic"><img src="${pageContext.request.contextPath}/img/2.jpg" alt=""></div>
            </div>
            <h1 class="head">create account</h1>
            <form action="#" class="form">
                <div>
                    <i class="fa fa-user-o"></i>
                    <input type="text" placeholder="用户名">
                </div>
                <div>
                    <i class="fa fa-envelope-o"></i>
                    <input type="email" placeholder="邮箱">
                </div>
                <div>
                    <i class="fa fa-key"></i>
                    <input type="password" placeholder="密码">
                </div>
                <div class="btn">
                    <button>注册</button>
                </div>
            </form>
            <p class="btn-something">
                已有账户? <span class="loginbtn">立即登录</span>
            </p>
        </div>
    </div>
</div>
</body>
<script>
    let login = document.querySelector(".login");
    let signup = document.querySelector(".signup");

    let loginbtn = document.querySelector(".loginbtn");
    let siginupbtn = document.querySelector(".signupbtn");

    let user = document.querySelector(".head");

    siginupbtn.addEventListener("click", () => {
        login.style.transform = "rotateY(180deg)"
        signup.style.transform = "rotateY(0deg)";
    })

    loginbtn.addEventListener("click", () => {
        login.style.transform = "rotateY(0deg)"
        signup.style.transform = "rotateY(-180deg)";
    })
</script>
</html>
