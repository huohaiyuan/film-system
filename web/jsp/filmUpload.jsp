<%--
  Created by IntelliJ IDEA.
  User: 26474
  Date: 2021/11/19
  Time: 10:50
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>filmUpload</title>
    <script src="https://unpkg.com/vue@2.6.11/dist/vue.js"></script>
    <script src="https://unpkg.com/element-ui@2.13.0/lib/index.js"></script>
    <link rel="stylesheet" href="//unpkg.com/element-ui@2.15.6/lib/theme-chalk/index.css">
</head>
<body>
<div id="upload">
    <el-container>
        <el-header style="height: 30px">
        <i class="el-icon-video-camera"></i>
        <el-divider direction="vertical"></el-divider>
        电影上传
    </el-header>
        <el-main style="padding: 0;border-radius: 15px">
            <el-card style="border-radius: 15px;height: 640px">
            <el-upload
                    class="upload-demo"
                    drag
                    action="../img"
                    multiple>
                <i class="el-icon-upload"></i>
                <div class="el-upload__text">将文件拖到此处，或<em>点击上传</em></div>
                <div class="el-upload__tip" slot="tip">只能上传jpg/png文件，且不超过500kb</div>
            </el-upload>
            </el-card>
        </el-main>
    </el-container>
</div>
</body>
<script>
    new Vue().$mount('#upload')
</script>
</html>
