<%--
  Created by IntelliJ IDEA.
  User: 26474
  Date: 2021/11/17
  Time: 14:19
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>firstPage</title>
    <script src="https://unpkg.com/vue@2.6.11/dist/vue.js"></script>
    <script src="https://unpkg.com/element-ui@2.13.0/lib/index.js"></script>
    <link rel="stylesheet" href="//unpkg.com/element-ui@2.15.6/lib/theme-chalk/index.css">
    <style>
        .el-carousel__item h3 {
            color: #475669;
            font-size: 14px;
            opacity: 0.75;
            line-height: 200px;
            margin: 0;
        }

        .el-carousel__item:nth-child(2n) {
            background-image: url("../img/3.png");
        }

        .el-carousel__item:nth-child(2n+1) {
            background-image: url("../img/4.png");
        }
    </style>
</head>
<body>
<div id="home">
    <el-container>
        <el-header style="height: 30px">
            <i class="el-icon-s-home"></i>
            <el-divider direction="vertical"></el-divider>
            首页
        </el-header>
        <el-main style="background-color: #ffffff;border-radius: 15px">
            <el-divider content-position="left">
                <span style="font-size: 20px">热播电影</span>
            </el-divider>
            <el-card>
                <el-carousel :interval="4000" type="card" height="200px">
                    <el-carousel-item v-for="item in 6">
                    </el-carousel-item>
                </el-carousel>
            </el-card>
            <el-divider content-position="left">
                <span style="font-size: 20px">精彩资讯</span>
            </el-divider>
            <el-card>
                <el-collapse accordion>
                    <el-collapse-item>
                        <template slot="title">
                            <el-badge value="hot">一致性 Consistency</el-badge>
                        </template>
                        <div>与现实生活一致：与现实生活的流程、逻辑保持一致，遵循用户习惯的语言和概念；</div>
                        <div>在界面中一致：所有的元素和结构需保持一致，比如：设计样式、图标和文本、元素的位置等。</div>
                    </el-collapse-item>
                    <el-collapse-item>
                        <template slot="title">
                            <el-badge value="hot">反馈 Feedback</el-badge>
                        </template>
                        <div>控制反馈：通过界面样式和交互动效让用户可以清晰的感知自己的操作；</div>
                        <div>页面反馈：操作后，通过页面元素的变化清晰地展现当前状态。</div>
                    </el-collapse-item>
                    <el-collapse-item>
                        <template slot="title">
                            效率 Efficiency
                        </template>
                        <div>简化流程：设计简洁直观的操作流程；</div>
                        <div>清晰明确：语言表达清晰且表意明确，让用户快速理解进而作出决策；</div>
                        <div>帮助用户识别：界面简单直白，让用户快速识别而非回忆，减少用户记忆负担。</div>
                    </el-collapse-item>
                    <el-collapse-item>
                        <template slot="title">
                            可控 Controllability
                        </template>
                        <div>用户决策：根据场景可给予用户操作建议或安全提示，但不能代替用户进行决策；</div>
                        <div>结果可控：用户可以自由的进行操作，包括撤销、回退和终止当前操作等。</div>
                    </el-collapse-item>
                </el-collapse>
            </el-card>
        </el-main>
    </el-container>
</div>
</body>
<script>
    new Vue().$mount('#home')
</script>
</html>
