<%--
  Created by IntelliJ IDEA.
  User: 26474
  Date: 2021/11/17
  Time: 20:25
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>filmMessage</title>
    <script src="https://unpkg.com/vue@2.6.11/dist/vue.js"></script>
    <script src="https://unpkg.com/element-ui@2.13.0/lib/index.js"></script>
    <link rel="stylesheet" href="//unpkg.com/element-ui@2.15.6/lib/theme-chalk/index.css">
    <style>
        .time {
            font-size: 13px;
            color: #999;
        }

        .bottom {
            margin-top: 13px;
            line-height: 12px;
        }

        .button {
            padding: 0;
            float: right;
        }

        .image {
            width: 100%;
            height: 100%;
            display: block;
        }

        .clearfix:before,
        .clearfix:after {
            display: table;
            content: "";
        }

        .clearfix:after {
            clear: both
        }
    </style>
    <style scoped>
        .time {
            font-size: 13px;
            color: #999;
        }

        .bottom {
            margin-top: 13px;
            line-height: 12px;
        }

        .button {
            padding: 0;
            float: right;
        }

        .image {
            width: 100%;
            display: block;
        }

        .clearfix:before,
        .clearfix:after {
            display: table;
            content: "";
        }

        .clearfix:after {
            clear: both
        }
    </style>
</head>
<body>
<div id="films">
    <el-container>
        <el-header style="height: 30px;">
            <i class="el-icon-video-camera"></i>
            <el-divider direction="vertical"></el-divider>
            电影库
        </el-header>
            <el-main style="padding: 0;border-radius: 15px">
                <el-card style="border-radius: 15px">
                <el-row style="border-radius: 15px">
                    <el-col style="margin-left:30px" :span="5" v-for="(item) in 8" :key="item.classId" :offset="1">
                        <div style="margin-top:50px">
                            <el-card style="border-radius: 15px" :body-style="{ padding: '15px'}" shadow="hover">
                                <img src="../img/3.png"
                                     class="image">
                                <div>
                                    <span>{{item.classGrade}}级{{item.classMajor}}系{{item.classNumber}}班</span><br>
                                    <div class="bottom clearfix">
                                        <time class="time"><strong>创建时间:</strong>{{item.classCreatetime}}</time>
                                        <el-button type="text" class="button" @click="add(item)">查看</el-button>
                                    </div>
                                </div>
                            </el-card>
                        </div>
                    </el-col>
                </el-row>
                <div style="text-align: center;padding-top: 90px;padding-bottom: 30px">
                    <el-pagination
                            background
                            layout="prev, pager, next"
                            :total="1000">
                    </el-pagination>
                </div>
                </el-card>
            </el-main>
    </el-container>
</div>
</body>
<script>
    new Vue().$mount('#films')
</script>
</html>
